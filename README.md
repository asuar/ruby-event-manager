# ruby-event-manager

Created with Ruby as part of The Odin Project [Curriculum](https://www.theodinproject.com/courses/ruby-programming/lessons/event-manager). 

# Final Thoughts

Completing this project allowed me to practice with Files, Serialization and APIs in Ruby.
