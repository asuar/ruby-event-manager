# frozen_string_literal: true

require 'csv'
require 'google/apis/civicinfo_v2'
require 'erb'

def clean_zipcode(zipcode)
  zipcode.to_s.rjust(5, '0')[0..4]
end

def legislators_by_zipcode(zip)
  civic_info = Google::Apis::CivicinfoV2::CivicInfoService.new
  civic_info.key = 'AIzaSyClRzDqDh5MsXwnCWi0kOiiBivP6JsSyBw'

  begin
    civic_info.representative_info_by_address(
      address: zip,
      levels: 'country',
      roles: %w[legislatorUpperBody legislatorLowerBody]
    ).officials
  rescue StandardError
    'You can find your representatives by visiting www.commoncause.org/take-action/find-elected-officials'
  end
end

def save_thank_you_letters(id, form_letter)
  Dir.mkdir('output') unless Dir.exist? 'output'
  filename = "output/thanks_#{id}.html"

  File.open(filename, 'w') do |file|
    file.puts form_letter
  end
end

def clean_phonenumber(phone_number)
  phone_number = phone_number.gsub(/\D/, '')

  if phone_number.length == 10
    return phone_number
  elsif phone_number.length == 11 && phone_number[0] == '1'
    return phone_number[1..10]
  else
    '0000000000'
  end
end

def clean_hour(date)
  time = DateTime.strptime(date, '%m/%d/%Y %H:%M')
  time.hour
end

def clean_day(date)
  time = DateTime.strptime(date, '%m/%d/%Y %H:%M')
  time.wday
end

puts 'EventManager Initialized!'

contents = CSV.open 'event_attendees.csv', headers: true, header_converters: :symbol

template_letter = File.read 'form_letter.erb'
erb_template = ERB.new template_letter

hours = Hash.new {}
days = Hash.new {}

contents.each do |row|
  id = row[0]
  name = row[:first_name]
  zipcode = clean_zipcode(row[:zipcode])
  legislators = legislators_by_zipcode(zipcode)
  phone_number = clean_phonenumber(row[:homephone])
  hour = clean_hour(row[:regdate])
  hours[hour] = if hours.include? hour
                  hours[hour] + 1
                else
                  1
                end
  day = clean_day(row[:regdate])
  days[day] = if days.include? day
                days[day] + 1
              else
                1
              end

  form_letter = erb_template.result(binding)
  save_thank_you_letters(id, form_letter)
end

# puts hours.key(hours.values.max)
# puts days.key(days.values.max)
